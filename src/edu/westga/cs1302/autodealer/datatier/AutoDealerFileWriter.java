package edu.westga.cs1302.autodealer.datatier;

import java.io.File;
import java.util.ArrayList;
import edu.westga.cs1302.autodealer.model.Automobile;
import edu.westga.cs1302.autodealer.resources.UI;

/**
 * The Class AlbumFileWriter.
 * 
 * @author CS1302
 */
public class AutoDealerFileWriter {

	private File inventoryFile;

	/**
	 * Instantiates a new auto file writer.
	 *
	 * @precondition inventoryFile != null
	 * @postcondition none
	 * 
	 * @param inventoryFile the inventory file
	 */
	public AutoDealerFileWriter(File inventoryFile) {
		if (inventoryFile == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.INVENTORY_FILE_CANNOT_BE_NULL);
		}

		this.inventoryFile = inventoryFile;
	}

	/**
	 * Writes all the autos in the dealership to the specified auto file. Each auto
	 * will be on a separate line and of the following format:
	 * make,model,year,miles,price
	 * 
	 * @precondition autos != null
	 * @postcondition none
	 * 
	 * @param autos The collection of autos to write to file.
	 */
	public void write(ArrayList<Automobile> autos) {
		if (autos == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.AUTOS_CANNOT_BE_NULL);
		}

		// TODO Complete this method as a part of Lab 2
	}

}
