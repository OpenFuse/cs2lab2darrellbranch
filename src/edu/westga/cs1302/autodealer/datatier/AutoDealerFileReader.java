package edu.westga.cs1302.autodealer.datatier;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import java.util.Scanner;

import edu.westga.cs1302.autodealer.model.Automobile;
import edu.westga.cs1302.autodealer.resources.UI;

/**
 * The Class AutoDealerFileReader. Reads an .adi (Auto Dealer Inventory) file which is a
 * CSV file with the following format:
 * make,model,year,miles,price
 * 
 * @author CS1302
 */
public class AutoDealerFileReader {
	public static final String FIELD_SEPARATOR = ",";

	private File inventoryFile;

	/**
	 * Instantiates a new auto dealer file reader.
	 *
	 * @precondition inventoryFile != null
	 * @postcondition none
	 * 
	 * @param inventoryFile
	 *            the inventory file
	 */
	public AutoDealerFileReader(File inventoryFile) {
		if (inventoryFile == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.INVENTORY_FILE_CANNOT_BE_NULL);
		}
		
		this.inventoryFile = inventoryFile;
	}

	/**
	 * Opens the associated adi file and reads all the autos in the file one
	 * line at a time. Parses each line and creates an automobile object and stores it in
	 * an ArrayList of Automobile objects. Once the file has been completely read the
	 * ArrayList of Automobiles is returned from the method. Assumes all
	 * autos in the file are for the same dealership.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return Collection of Automobile objects read from the file.
	 */
	public ArrayList<Automobile> loadAllAutos() {
		
		ArrayList<Automobile> autos = new ArrayList<Automobile>();
		
		try (Scanner in = new Scanner(this.inventoryFile)) {
			in.useDelimiter(",|\\r\\n|\n");
			
			while (in.hasNext()) {
				String line = in.nextLine();
				
				String[] data = line.split(",");
				
				String make = data[0];
				String model = data[1];
				Integer year = Integer.parseInt(data[2]);
				Double miles = Double.parseDouble(data[3]);
				Double price = Double.parseDouble(data[4]);
				
				Automobile auto = new Automobile(make, model, year, miles, price);
				
				autos.add(auto);
				
			}
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		return autos;  
	}

}
